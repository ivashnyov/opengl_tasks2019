#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <vector>
#include <iostream>
#include <queue>

#include <glm/gtc/constants.hpp>


MeshPtr makeCone(float height, float radius, int nPolygones)
{
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;

    glm::vec3 coneTop = glm::vec3(0.0f, 0.0f, height);

    for(int i = 0; i < nPolygones; ++i) {
        float angle1 = glm::two_pi<float>() * i / nPolygones;
        glm::vec3 v1 = radius * glm::vec3(glm::cos(angle1), glm::sin(angle1), 0.0f);

        float angle2 = glm::two_pi<float>() * (i + 1) / nPolygones;
        glm::vec3 v2 = radius * glm::vec3(glm::cos(angle2), glm::sin(angle2), 0.0f);

        // боковая поверхность
        vertices.push_back(v1);
        vertices.push_back(v2);
        vertices.push_back(coneTop);

        normals.push_back(glm::normalize(v1));
        normals.push_back(glm::normalize(v2));
        normals.push_back(glm::normalize(coneTop));

        // основание
        vertices.push_back(v1);
        vertices.push_back(v2);
        vertices.emplace_back(glm::vec3(0.0f, 0.0f, 0.0f));

        normals.emplace_back(glm::vec3(0.0f, 0.0f, -1.0f));
        normals.emplace_back(glm::vec3(0.0f, 0.0f, -1.0f));
        normals.emplace_back(glm::vec3(0.0f, 0.0f, -1.0f));
    }

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    std::cout << "Cone is created with " << vertices.size() << " vertices\n";

    return mesh;
}


class TreeBuilder {
public:

    void Build( std::vector<MeshPtr>& meshes )
    {
        build( 1, 0.1, 3, glm::translate(glm::mat4( 1.0f ), glm::vec3(0.0f, 0.0f, -1.0f) ), meshes );
    }

private:
    void build( int depth, float radius, float length, glm::mat4 mat, std::vector<MeshPtr>& meshes )
    {
        if( depth > 5 ) {
            return;
        }

        int n = 100 / (depth * glm::log(1 + depth) );

        MeshPtr cone = makeCone( length, radius, n );
        cone->setModelMatrix( mat );
        meshes.push_back( cone );

        for( int level = 1; level <= 3; ++level ) {
            int n_children = 3 + rand() % 3;
            for( int i = 0; i < n_children; ++i ) {
                float h = length * ( 0.15f + 0.2f * level + 1.0f * (rand() % 20) / 100 );
                float r = radius * ( 1.0f - h / length) * (0.9f - 1.0f * (rand() % 20) / 100 );
                float l = length * ( 0.7f - 1.0f * (rand() % 20) / 100 ) / level;
                float alpha = glm::radians( 360.0f * i / n_children + rand() % 40 );
                float beta = glm::radians( 30.0f + rand() % 10 );

                glm::mat4 childMat = glm::translate( glm::mat4( 1.0f ), glm::vec3( 0.0f, 0.0f, h ) );
                childMat = glm::rotate( childMat, alpha, glm::vec3( 0.0f, 0.0f, 1.0f ) );
                childMat = glm::rotate( childMat, beta, glm::vec3( 1.0f, 0.0f, 0.0f ) );

                build( depth + level, r, l, mat * childMat, meshes );
            }
        }
    }
};


class TreeApplication : public Application {
public:
    std::vector<MeshPtr> _tree;

    ShaderProgramPtr _shader;

    void makeScene() override
    {
        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();

        TreeBuilder().Build( _tree );

        _shader = std::make_shared<ShaderProgram>("591BelkinData/shader.vert", "591BelkinData/shader.frag");
    }

    void update() override
    {
        Application::update();
    }

    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Устанавливаем шейдер.
        _shader->use();

        //Устанавливаем общие юниформ-переменные
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        for( MeshPtr mp : _tree ) {
            _shader->setMat4Uniform("modelMatrix", mp->modelMatrix());
            mp->draw();
        }
    }
};

int main()
{
    srand( 42 );
    TreeApplication app;
    app.start();

    return 0;
}