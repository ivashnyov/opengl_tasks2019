set(SRC_FILES
    common/Application.hpp
    common/Application.cpp
    common/Camera.hpp
    common/Camera.cpp
    common/Common.h
    common/DebugOutput.h
    common/DebugOutput.cpp
    common/Mesh.hpp
    common/Mesh.cpp
    common/ShaderProgram.hpp
    common/ShaderProgram.cpp
    Main.h
    Main.cpp
)

MAKE_OPENGL_TASK(597Zuev 1 "${SRC_FILES}")