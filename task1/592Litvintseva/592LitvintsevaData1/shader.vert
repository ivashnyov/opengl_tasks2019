#version 330

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;
//layout(location = 2) in float N;

out vec4 color;

void main()
{
    float h = vertexPosition[2];
    float red_dist = 2*(0.5 - h)*int(h < 0.5);
    float green_dist = 2*h*int(h <= 0.5) + (2 - 2*h)*int(h > 0.5);
    float blue_dist = 2*(h - 0.5)*int(h > 0.5);

    color.rgb = vec3(blue_dist, green_dist, red_dist);
    color.a = 1;

    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
}